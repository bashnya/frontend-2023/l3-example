let orderText;

function createOrderText(coffeType, sugarAmount, isMilkAdded) { 
    let message = `Вы заказали ${coffeType}.`;

    if (sugarAmount > 0) {
        message += ` Добавили ${sugarAmount} кубик(а) сахара.`;
    }

    if(isMilkAdded) {
        message += ' Добавили молоко.';
    }
 
    return message;
}

function hanleFormSubmit(event) { 
    event.preventDefault();
    
    const coffeType = document.querySelector('#coffe-type').value;
    const sugarAmount = document.querySelector('#sugar').value;
    const isMilkAdded = document.querySelector('#milk').checked;

    orderText = createOrderText(coffeType, sugarAmount, isMilkAdded);

    document.querySelector('#order-message').textContent = orderText;
}

const button = document.querySelector('button');
button.addEventListener('click', hanleFormSubmit);

document.querySelector('#order-message').addEventListener('click', () => {
    alert(orderText);
})