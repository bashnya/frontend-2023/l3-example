# БАШНЯ. Курс по фронту. Код к занятию 3

### На занятии мы разбирали основы JS, а именно:

* Переменные и их область видимости
* Типы данных
* Функции, их аргументы и возвращаемые значения
* Работа с DOM - получение информации из элементов страницы и её добавление
* События
* Обработчики событий

### Полезные ссылки, которыми вы можете воспользоваться для углублённого изучения:

* https://www.w3schools.com/ 
    Один из лучших сайтов с туториалами, за счёт оч функциональной песочницы. Можно прямо на сайте потыкаться, поменять представленный код и посмотреть, к чему это приведёт
* https://learn.javascript.ru/
    Приятный учебник по js на русском. Если вам нужен именно формат структорированного "чёпочитать" - это для вас.
* https://developer.mozilla.org/ru/ 
    Один из наиболее используемых сайтов, когда надо подглянуть что-то конкретное. Тоже есть песочница, но попроще. По большинству методов приведены таблички, в каких браузерах и с чем это работает, часто ради этих табличек и заглядываю.